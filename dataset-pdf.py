import jinja2
import os
import subprocess
import urllib.request as request
import json
from datetime import date
from datetime import datetime
import traceback
import logging

# string for URL to request package information
base_url = 'https://gis-hub.ca/api/3/action/package_show?id='

# current working directory
working_dir = os.getcwd()

# latex template document
latex_template = 'dataset-template.tex'

# date (MM/DD/YYYY - 06/14/2021)
date_time = datetime.now().strftime('%m/%d/%Y')

# Jinja2 env
latex_jinja_env = jinja2.Environment(
    block_start_string='\BLOCK{',
    block_end_string='}',
    variable_start_string='\VAR{',
    variable_end_string='}',
    comment_start_string='\#{',
    comment_end_string='}',
    line_statement_prefix='%%',
    line_comment_prefix='%#',
    trim_blocks=True,
    autoescape=False,
    loader=jinja2.FileSystemLoader(os.path.abspath('.'))
)


def set_outdir(outdir_name):
    if not os.path.exists(os.path.join(os.getcwd(), outdir_name)):
        print(f'Changing directory to: {os.path.join(os.getcwd(), outdir_name)}')
        os.mkdir(outdir_name)
        os.chdir(outdir_name)
        return outdir_name
    else:
        print(f'Changing directory to: {os.path.join(os.getcwd(), outdir_name)}')
        os.chdir(outdir_name)
        return outdir_name


# get json metadata results using CKAN API
def get_results(dataset_name):
    try:
        logging.info(f'Retrieving metadata for record with name: {dataset_name}')
        response = request.urlopen(base_url + dataset_name)
        html = response.read()
        result = json.loads(html.decode('utf-8')).get('result')
        return result
    except:
        logging.error(f'Error retrieving metadata for record with name {dataset_name}')
        logging.error(traceback.format_exc())


# https://stackoverflow.com/questions/6116978/how-to-replace-multiple-substrings-of-a-string
def replace_all(text, dict):
    """
    For each item in dict, search the key in the text string and replace with the corresponding dict value.
    :param text: string of text
    :param dict: dictionary with key (string to be replaced) and value (string to replace current string with)
    :return: modified string with replaced text
    """
    for i, j in dict.items():
        text = text.replace(i, j)
    return text


def clean_results(metadata_results):
    try:
        logging.info(f"Cleaning {metadata_results.get('title')} metadata for LaTeX format...")
        fields_to_clean = ['title', 'notes', 'keywords', 'science_keywords', 'citation', 'co_creators', 'references',
                           'uncertainties', 'collaboration', 'logical_consistency', 'completeness', 'methods',
                           'spatial_data_quality', 'temporal_coverage', 'update_frequency', 'theme',
                           'scripts_or_software_routines', 'positional_accuracy', 'absence_data']

        # characters to map to (LaTeX formatting)
        # https://www.stevesque.com/symbols/
        char_map = {"_": " ",
                    "’": "'",
                    "\r\n": "",
                    "&": r"\&",
                    "%": r"\%",
                    "^": r"\^{}",
                    "#": r"\#",
                    "≥": "$\geq$",
                    ">": "$>$",
                    "≤": "$\geq$",
                    "<": "$<$",
                    "±": "$\pm$",
                    "×": r"$\times$",
                    "÷": r"$\div$",
                    "∼": r"$\sim$"}

        for field in fields_to_clean:
            if field == 'references':
                ref_list = metadata_results.get('references')
                if ref_list:
                    metadata_results[field] = [replace_all(ref.get('reference'), char_map) for ref in
                                               json.loads(ref_list)]
                else:
                    metadata_results[field] = ['None provided.']
            elif field in ('keywords', 'science_keywords'):
                klist = metadata_results.get(field).split(',')
                klist_cleaned = [replace_all(k, char_map) for k in klist]
                metadata_results[field] = ', '.join(klist_cleaned)
            else:
                metadata_results[field] = replace_all(metadata_results.get(field), char_map)
        return metadata_results
    except:
        logging.error(f'Error cleaning {metadata_results} metadata...')
        logging.error(traceback.format_exc())


# logger setup function
def logger_setup(filename):
    logging.basicConfig(
        filename=filename,
        format="| %(levelname)-7s| %(asctime)s | %(message)s",
        level="INFO",  # numeric value: 20
    )
    log = logging.getLogger('dataset-pdf')
    return log


# create output directory
outdir = set_outdir('output')

# set up logger
logger = logger_setup(f"metadata-log-{datetime.now():%Y-%m-%d-%H:%M:%S}.log")


# class to contain general information and metadata pulled from open maps records
class MetaData(object):
    def __init__(self, results, out_dir):
        # Doc Info
        self.rheader = 'Pacific Science, DFO'
        self.lheader = 'GIS Hub'
        self.doc_title = results.get('title')
        self.date_text = f"Date Retrieved: {date.today().strftime('%B %-d, %Y')}"
        self.author = results.get('organization').get('title')

        def get_name(results_dict):
            logger.info('Getting title...')
            return results_dict.get('title')

        def get_maintainer(results_dict):
            logger.info('Getting maintainer...')
            return results_dict.get('maintainer_email')

        def get_summary(results_dict):
            logger.info('Getting summary (description)...')
            return results_dict.get('notes')

        def get_topic(results_dict):
            logger.info('Getting topic...')
            return results_dict.get('topic_category')

        def get_keywords(results_dict):
            logger.info('Getting list of keywords...')
            return results_dict.get('keywords')

        def get_science_keywords(results_dict):
            logger.info('Getting list of science keywords...')
            return results_dict.get('science_keywords')

        def get_citation(results_dict):
            logger.info('Getting citation...')
            return results_dict.get('citation')

        def get_references(results_dict):
            logger.info('Getting references...')
            return results_dict.get('references')

        def get_cocreators(results_dict):
            logger.info('Getting co-creators...')
            return results_dict.get('co_creators')

        def get_collaboration(results_dict):
            logger.info('Getting collaboration...')
            return results_dict.get('collaboration')

        def get_uncertainties(results_dict):
            logger.info('Getting uncertainties...')
            return results_dict.get('uncertainties')

        def get_logical_consistency(results_dict):
            logger.info('Getting logical consistency...')
            return results_dict.get('logical_consistency')

        def get_completeness(results_dict):
            logger.info('Getting completeness...')
            return results_dict.get('completeness')

        def get_methods(results_dict):
            logger.info('Getting methods...')
            return results_dict.get('methods')

        def get_data_quality(results_dict):
            logger.info('Getting spatial data quality...')
            return results_dict.get('spatial_data_quality')

        def get_temporal_coverage(results_dict):
            logger.info('Getting temporal coverage...')
            return results_dict.get('temporal_coverage')

        def get_update_frequency(results_dict):
            logger.info('Getting update frequency...')
            return results_dict.get('update_frequency')

        def get_theme(results_dict):
            logger.info('Getting theme...')
            return results_dict.get('theme')

        def get_scripts(results_dict):
            logger.info('Getting scripts or software routines...')
            return results_dict.get('scripts_or_software_routines')

        def get_use_restrictions(results_dict):
            logger.info('Getting use restrictions...')
            return results_dict.get('use_restrictions')

        def get_start_date(results_dict):
            logger.info('Getting start date...')
            return results_dict.get('start_date')

        def get_end_date(results_dict):
            logger.info('Getting end date...')
            return results_dict.get('end_date')

        def get_positional_accuracy(results_dict):
            logger.info('Getting positional accuracy...')
            return results_dict.get('positional_accuracy')

        def get_absence_data(results_dict):
            logger.info('Getting absence data...')
            return results_dict.get('absence_data')

        def get_date_completed(results_dict):
            logger.info('Getting date completed...')
            return results_dict.get('date_completed')

        def get_date_published(results_dict):
            logger.info('Getting date published...')
            return results_dict.get('date_published')

        def get_data_creator(results_dict):
            logger.info('Getting data creator...')
            creator_dict = json.loads(results_dict.get('data_creator'))
            return f"{creator_dict.get('creator_name')} ({creator_dict.get('creator_email')}) - {creator_dict.get('creator_organization')}"

        def get_program_manager(results_dict):
            logger.info('Getting program manager...')
            mgr_dict = json.loads(results_dict.get('program_manager'))
            return f"{mgr_dict.get('manager_name')} ({mgr_dict.get('manager_email')}) - {mgr_dict.get('manager_organization')}"

        def get_num_resources(results_dict):
            logger.info('Getting number of resources...')
            return results_dict.get('num_resources') - 1

        def get_res_formats(results_dict):
            logger.info('Getting resource formats...')
            resources = results_dict.get('resources')
            return ', '.join(set([res.get('format') for res in resources[1:]]))

        def get_disclaimer(results_dict):
            logger.info('Getting disclaimer...')
            resources = results_dict.get('resources')
            return ', '.join(set([res.get('disclaimer') for res in resources]))

        def convert_to_pdf(tex_template, results_dict):
            # latex template file
            logger.info('Loading template...')
            template = latex_jinja_env.get_template(tex_template)

            # write new latex document from template with metadata
            datestamp = f"{datetime.now():%Y-%m-%d}"
            doc_name = results_dict.get('title').lower().replace(' ', '_').replace('-', '_').replace('__', '_')[:30]
            latex_doc = f"{doc_name}_{datestamp}.tex"
            logger.info(f'Populating template with metadata and producing output file: {latex_doc}')

            with open(latex_doc, 'w') as tex_file:
                tex_file.write(template.render(rheader=self.rheader,
                                               lheader=self.lheader,
                                               title=self.doc_title,
                                               author=self.author,
                                               date=self.date_text,
                                               # Basic
                                               notes=self.summary,
                                               maintainer=self.maintainer,
                                               citation=self.citation,
                                               start_date=self.start_date,
                                               end_date=self.end_date,
                                               # Contact
                                               creator=self.creator,
                                               manager=self.program_manager,
                                               # General
                                               topic_category=self.topic,
                                               date_completed=self.date_completed,
                                               date_published=self.date_published,
                                               update_frequency=self.update_frequency,
                                               keywords=self.keywords,
                                               # Science
                                               science_keywords=self.science_keywords,
                                               theme=self.theme,
                                               methods=self.methods,
                                               scripts=self.scripts,
                                               spatial_quality=self.spatial_quality,
                                               positional_accuracy=self.positional_accuracy,
                                               logical_consistency=self.logical_consistency,
                                               use_restrictions=self.use_restrictions,
                                               temporal_coverage=self.temporal_coverage,
                                               references=self.references,
                                               # Resources
                                               collaboration=self.collaboration,
                                               num_resources=self.num_resources,
                                               res_formats=self.res_formats,
                                               disclaimer=self.disclaimer
                                               )
                               )

            # convert latex output to PDF file
            logger.info('Converting LaTeX output to PDF file.')
            # https://tex.stackexchange.com/questions/258814/what-is-the-difference-between-interaction-nonstopmode-and-halt-on-error
            return subprocess.call(['pdflatex', latex_doc, 'interaction=nonstopmode'])

        # Basic Info
        self.name = get_name(results)
        self.summary = get_summary(results)
        self.maintainer = get_maintainer(results)
        self.citation = get_citation(results)
        self.start_date = get_start_date(results)
        self.end_date = get_end_date(results)

        # Contact Info
        self.creator = get_data_creator(results)
        self.co_creators = get_cocreators(results)
        self.program_manager = get_program_manager(results)

        # General Info
        self.topic = get_topic(results)
        self.date_completed = get_date_completed(results)
        self.date_published = get_date_published(results)
        self.update_frequency = get_update_frequency(results)
        self.keywords = get_keywords(results)

        # Science Info
        self.science_keywords = get_science_keywords(results)
        self.theme = get_theme(results)
        self.methods = get_methods(results)
        self.scripts = get_scripts(results)
        self.spatial_quality = get_data_quality(results)
        self.positional_accuracy = get_positional_accuracy(results)
        self.logical_consistency = get_logical_consistency(results)
        self.completeness = get_completeness(results)
        self.absence_data = get_absence_data(results)
        self.uncertainties = get_uncertainties(results)
        self.use_restrictions = get_use_restrictions(results)
        self.temporal_coverage = get_temporal_coverage(results)
        self.references = get_references(results)
        self.collaboration = get_collaboration(results)

        # Resources
        self.num_resources = get_num_resources(results)
        self.res_formats = get_res_formats(results)
        self.disclaimer = get_disclaimer(results)

        # Latex
        self.pdf = convert_to_pdf(latex_template, results)


def main():
    # CLI arguments
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("dataset_name", type=str, help="Name of dataset on GIS Hub.")
    args = parser.parse_args()

    metadata_results = get_results(args.dataset_name)
    cleaned_results = clean_results(metadata_results)

    # create object from metadata class
    meta_object = MetaData(cleaned_results, outdir)


if __name__ == "__main__":
    main()
