# GIS Hub Dataset Metadata Report

__Main author:__  Cole Fields  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel: 250-363-8060

- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)

## Objective

The purpose of this project is to generate a PDF version of a GIS Hub dataset's metadata record.

## Summary

Dataset metadata can be saved as a txt file or a json file easily. However, a formatted PDF version is easier to read.
This is sometimes desired if we want to share the metadata with a client, but not the spatial files.

## Status

In-development

## Contents

##### dataset-template.tex

LaTeX template document. This defines the structure of the document and includes variables that need to be populated
with metadata text. The template is where sections should be added/removed.

##### dataset-pdf.py

Python script to be executed from the command line using Python3. Contains the functions required to get dataset
metadata from the GIS Hub and convert it to a PDF document. One argument required, the name of the GIS Hub dataset.

## Methods

##### dataset-template.tex

- Loads required packages.
- Defines elements of documents such as title, author, headers.
- Create a block that will iterate over the list of datasets, and define variables to loop over the lists of elements
  that will be passed from python script.

##### dataset-pdf.py

- Defines functions to get dataset metadata.
- Creates output directory.
- Starts a log file.
- Creates a general information dictionary with text for things like the title, author, headers, date.
- Sets the Jinja environment.
- Defines a class with attributes and a number of functions including getting metadata. It also loads the template LaTeX document, and then
  populates it with metadata from the GIS Hub record. Finally, the LaTeX output is converted to PDF format.
- Creates an object of the custom class with all the required metadata.
- Loads LaTeX template and populates it using Jinja and the metadata object to create a new LaTeX file.
- Converts new LaTeX file to PDF.

## Requirements

##### Install LaTeX on your machine

* Download [here](https://miktex.org/download)

##### Clone repository

* Open Git Bash, or another shell and `cd` to where you want the project to be copied to.

* Type `git clone git@gitlab.com:dfo-msea/gis-hub/metadata-pdf.git`. This will clone the repository.

##### Create new python virtual environment

* `pip3 -m venv /home/cole/.virtualenvs/latex` set the name to whatever you'd like.

##### Activate new virtual environment & install packages

* Type `source </path/to/.virtualenvs>/latex/bin/activate` such as `source /home/cole/.virtualenvs/latex/bin/activate`

* You should see the name of the virtual environment in parentheses in your shell. This means the env is activated.

* `cd` into the cloned directory `cd metadata-pdf`.

* Type `pip3 install -r requirements.txt`.

##### Run code

* In a shell with new env (with required packages) activated, `cd` into directory with code (`metadata-pdf`)
  if not already there.

* Type `python dataset-pdf.py <dataset_name>` to run the python script, specifying the dataset name (i.e. 'seamounts').

## Caveats

* Will not run without LaTeX installed on your computer.

## Uncertainty

*Optional section.* Is there some uncertainty associated with the output? Assumptions that were made?

## Acknowledgements

*Optional section.* List any contributors and acknowledge relevant people or institutions

## References

* https://jinja.palletsprojects.com/en/2.11.x/

* https://stackoverflow.com/questions/22181944/using-utf-8-characters-in-a-jinja2-template
